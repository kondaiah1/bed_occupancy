import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoginService } from './service/login.service';
import { Router } from '@angular/router';
import { StorageService } from '../core/auth/storage/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private storageService: StorageService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    // if form is valid
    if (this.loginForm.valid) {
      let loginDetail:any = {}
      loginDetail.username = this.loginForm.value.userName;
      loginDetail.password = this.loginForm.value.password;
      this.loginService.getLogin(loginDetail).subscribe((response) => {
        if(response && response.status && response.status === "Success") {
          this.storageService.setSessionStorage('isLoggedin', 'true'); //set the value in session storage
          this.router.navigate(['/']); //navigate to dashboard
        }
      }, (error) => {
        console.error(error);
      })
    }
  }

}
