import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  //call login method
  getLogin(payload:any): Observable<any> {
    return this.http.post<any>(environment.loginURL + '/logIn', payload);
  }
  
}
