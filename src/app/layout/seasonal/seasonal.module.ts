import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeasonalRoutingModule } from './seasonal-routing.module';
import { SeasonalComponent } from './seasonal.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SeasonalComponent],
  imports: [
    CommonModule,
    SeasonalRoutingModule,
    SharedModule
  ]
})
export class SeasonalModule { }
