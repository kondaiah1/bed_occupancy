import { Component, OnInit } from '@angular/core';
import { boxWhiskerChartObject } from '../common/models/charts/boxandwhisker';
import { StorageService } from 'src/app/core/auth/storage/storage.service';
import { seasonalGraphsArray } from './constant/seasonal';
import { bedDetailsObject } from '../common/models/beddetails';

@Component({
  selector: 'app-seasonal',
  templateUrl: './seasonal.component.html',
  styleUrls: ['./seasonal.component.scss']
})
export class SeasonalComponent implements OnInit {
  public bedDetailsObject:any = new bedDetailsObject();
  public chartObject:any;
  private graphData:any;
  private graphArr:any = seasonalGraphsArray;
  private DayHourGraph:any;
  private YearMonthGraph:any;
  private HolidayKidGraph:any;

  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.graphData = JSON.parse(this.storageService.getSessionStorage('graphData'));
    if (this.graphData) {
      this.seasonalBedSuccess(this.graphData);
    }
    this.chartObject = this.getGraphShow('', 'Hour_of_the_day');
  }

  private getGraphShow(graphObj:any, graphLabel:any) {
    switch (graphLabel) {
        case 'Hour_of_the_day':
            return this.DayHourGraph = this.DayHourGraph ? this.DayHourGraph : graphObj;
        case 'Month_of_the_year':
            return this.YearMonthGraph = this.YearMonthGraph ? this.YearMonthGraph : graphObj;
        case 'Holidays_for_Kids':
            return this.HolidayKidGraph = this.HolidayKidGraph ? this.HolidayKidGraph : graphObj;
    }
  }

  //making graph data object after API response
  private setGraphValue(graphItem:any, graphObj:any, chartCategory:any, data:any) {
    graphObj.dataSource.chart.caption = "";
    graphObj.dataSource.chart.subcaption = "";
    graphObj.dataSource.chart.xAxisName = graphItem.xAxisName;
    graphObj.dataSource.chart.YAxisName = graphItem.yAxisName;
    graphObj.dataSource.chart.theme = "fusion";
    graphObj.dataSource.chart.showLegend = 1;
    graphObj.dataSource.chart.legendPosition = "bottom";
    graphObj.dataSource.chart.interactiveLegend = 0;
    graphObj.dataSource.chart.legendIconSides = 4;
    graphObj.dataSource.categories[0].category = chartCategory;
    graphObj.dataSource.dataset[0].lowerboxcolor = "#0075c2";
    graphObj.dataSource.dataset[0].upperboxcolor = "#1aaf5d";
    graphObj.dataSource.dataset[0].data = data;
    //set graph data based up on charts
    this.getGraphShow(graphObj, graphItem.xLabel);
  }

  private seasonalBedSuccess(response:any) {
    //bed details data object after API response
    this.bedDetailsObject.seriesTransaction = response.number_of_transactions;
    this.bedDetailsObject.parameterAnalyzed = response.number_of_parameters;
    this.bedDetailsObject.bedGreater = response.number_of_beds_more_than_70;
    this.bedDetailsObject.bedLesser = response.number_of_beds_less_than_70;
    
    for (const graphItem of this.graphArr) {
        let graphObj = new boxWhiskerChartObject();
        let chartCategory:any = [],
            data:any = [];
        response[graphItem.xLabel].filter((element:any, hourIndex:number, array:any) => {
          let elementVal = element.toString();
            if(array.indexOf(element) === hourIndex) {
                chartCategory.push({ label: elementVal });
                data.push({
                    value: (response[graphItem.yLabel][hourIndex]).toString()
                })
            } else {
                let getChartCategoryIndex = chartCategory.findIndex((item:any) => item.label === elementVal);
                data[getChartCategoryIndex].value = data[getChartCategoryIndex].value + ',' + response[graphItem.yLabel][hourIndex];
            }
        });
        //making graph data object after API response
        this.setGraphValue(graphItem, graphObj, chartCategory, data);
    }
  }

  //click event of previous and next
  nextPrev(eventName:any) {
    let graphObj:any,
        index:any = this.graphArr.findIndex((item:any) => item.xAxisName === this.chartObject.dataSource.chart.xAxisName);
    if (eventName === 'prev' && index) {
        index--;
        graphObj = this.getGraphShow('', this.graphArr[index].xLabel);
    } else if (eventName === 'next' && (index < this.graphArr.length - 1)) {
        index++;
        graphObj = this.getGraphShow('', this.graphArr[index].xLabel);
    }
    return this.chartObject = graphObj ? graphObj : this.chartObject;
  }

}
