import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  //call Get Bed Occupancy method
  getBedOccupancy(): Observable<any> {
    return this.http.get<any>(environment.URL + '/bed_occupancy_predict');
  }

}
