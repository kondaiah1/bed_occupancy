import { Component, OnInit } from '@angular/core';
import { lineChartObject } from '../common/models/charts/line';
import { DashboardService } from './service/dashboard.service';
import { StorageService } from 'src/app/core/auth/storage/storage.service';
import { bedDetailsObject } from '../common/models/beddetails';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public bedDetailsObject:any = new bedDetailsObject();
  public chartObject:any = new lineChartObject(); //line chart object from model
  private graphData:any;
  
  constructor(private dashboardService: DashboardService,
    private storageService: StorageService) { }

  ngOnInit() {
    this.graphData = JSON.parse(this.storageService.getSessionStorage('graphData'));
    //getting data about bed Occupancy
    this.graphData ? this.bedOccupancySuccess(this.graphData) : this.bedOccupancy();
  }

  //after success of bed occupancy API
  bedOccupancySuccess(data:any) {
    let chartCategory = [],
      dataSet = [],
      seriesName = ['actual', 'predicted'];

    //bed details data object after API response
    this.bedDetailsObject.seriesTransaction = data.number_of_transactions;
    this.bedDetailsObject.parameterAnalyzed = data.number_of_parameters;
    this.bedDetailsObject.bedGreater = data.number_of_beds_more_than_70;
    this.bedDetailsObject.bedLesser = data.number_of_beds_less_than_70;

    //making graph data object after API response
    seriesName.forEach((seriesName:string, seriesIndex:number) => {
      dataSet.push({
        seriesName: seriesName.charAt(0).toUpperCase() + seriesName.slice(1),
        data: []
      });
      data[seriesName].forEach((item:number, index:string) => {
        (chartCategory.length !== data[seriesName].length) ? chartCategory.push({ label: index.toString() }) : chartCategory;
        dataSet[seriesIndex].data.push({ value: item });
      });
    });
    this.chartObject.dataSource.chart.caption = "Bed Occupancy";
    this.chartObject.dataSource.chart.subcaption = "";
    this.chartObject.dataSource.chart.xAxisName = "Instance Number";
    this.chartObject.dataSource.chart.YAxisName = "Free Bed";
    this.chartObject.dataSource.chart.drawAnchors = 0;
    this.chartObject.dataSource.chart.showLegend = 1;
    this.chartObject.dataSource.chart.interactiveLegend = 0;
    this.chartObject.dataSource.chart.legendPosition = "bottom";
    this.chartObject.dataSource.chart.legendIconSides = 4;
    this.chartObject.dataSource.chart.plotHighlightEffect = "fadeout|anchorBgColor=ff0000, anchorBgAlpha=50";
    this.chartObject.dataSource.chart.theme = "fusion";
    this.chartObject.dataSource.categories[0].category = chartCategory;
    this.chartObject.dataSource.dataset = dataSet;
  }

  //Calling Bed Occupancy API
  bedOccupancy() {
    this.dashboardService.getBedOccupancy().subscribe((response) => {
      if (response) {
        this.storageService.setSessionStorage('graphData', JSON.stringify(response));
        this.bedOccupancySuccess(response);
      }      
    }, (error) => {
      console.error(error);
    });    
  }

}
