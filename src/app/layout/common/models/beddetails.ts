export class bedDetailsObject {
    seriesTransaction?: number;
    parameterAnalyzed?: number;
    bedGreater?: number;
    bedLesser?: number;
}