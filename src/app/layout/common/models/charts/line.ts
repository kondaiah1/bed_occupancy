//chart object
export class lineChartObject {
    width = "100%";
    height = "400";
    type = "msline";
    dataFormat = "JSON";
    dataSource = new dataSource();
}

//datasource object
export class dataSource {
    chart = new chartOption();
    categories: categoryOption[] = new Array(new categoryOption());
    dataset = [];
}

//chart options
export class chartOption {
    bgColor: string = "#000000";
    bgAlpha: string = "90";
    drawAnchors:number;
    caption: string;
    subcaption: string;
    xAxisName: string;
    YAxisName: string;
    numberPrefix: string;
    numberSuffix: string;
    theme: string;
    legendPosition: string;
    showLegend: number;
    interactiveLegend: number;
    legendIconSides: number;
    plotHighlightEffect: string;
    baseFontColor: string = "#ffffff";
    labelFontColor: string = "#ffffff";
}

//chart category options
export class categoryOption {
    category:any = [];
}