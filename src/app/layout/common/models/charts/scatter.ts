//chart object
export class scatterChartObject {
    width = "100%";
    height = "400";
    type = "selectscatter";
    dataFormat = "JSON";
    dataSource = new dataSource();
}

//datasource object
export class dataSource {
    chart = new chartOption();
    categories: categoryOption[] = new Array(new categoryOption());
    dataset: dataSet[] = new Array(new dataSet());
}

//chart options
export class chartOption {
    bgColor: string = "#000000";
    bgAlpha: string = "90";
    caption: string;
    subcaption: string;
    xAxisName: string;
    YAxisName: string;
    numberPrefix: string;
    numberSuffix: string;
    theme: string;
    legendPosition: string;
    showLegend: number;
    interactiveLegend: number;
    legendIconSides: number;
    plotHighlightEffect: string;
    baseFontColor: string = "#ffffff";
    labelFontColor: string = "#ffffff";
}

//chart category options
export class categoryOption {
    verticallinealpha: string;
    category:any = [];
}

//chart dataset
export class dataSet {
    drawline: string;
    seriesname: string;
    color: string;
    anchorsides: number;
    anchorradius: number;
    anchorbgcolor: string;
    anchorbordercolor: string;
    data:any = [];
}