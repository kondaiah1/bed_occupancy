import { Component, OnInit } from '@angular/core';
import { SidenavService } from '../../../service/sidenav/sidenav.service';
import { StorageService } from '../../../../core/auth/storage/storage.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private sideNavService: SidenavService,
    private authService: AuthService) { }

  ngOnInit() {
  }

  clickMenu() {
    this.sideNavService.toggle();
  }

  logout() {
    this.authService.logout();
  }

}
