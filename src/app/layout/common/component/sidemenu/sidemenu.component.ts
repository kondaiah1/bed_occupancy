import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/core/auth/service/auth.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit {
  @Input() sidenavmenu: any;
   checkSideMenu:boolean;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    
  }
  
  //logout functiom
  logout() {
    this.authService.logout();
  }
}
