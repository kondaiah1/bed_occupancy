import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { StorageService } from '../../../../core/auth/storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService implements CanActivate {

  constructor(private router: Router,
    private storageService: StorageService) { }
  
  canActivate() {
    let checkLoggedin = this.storageService.getSessionStorage('isLoggedin');
    return checkLoggedin ? true : this.router.navigate(['/login']);
  }
}
