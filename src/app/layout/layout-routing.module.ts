import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';


const routes: Routes = [{ path: '', component: LayoutComponent, loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
    { path: 'home', component: LayoutComponent, loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
    { path: 'season', component: LayoutComponent, loadChildren: () => import('./seasonal/seasonal.module').then(m => m.SeasonalModule) },
    { path: 'climate', component: LayoutComponent, loadChildren: () => import('./climate/climate.module').then(m => m.ClimateModule) },
    { path: 'patient', component: LayoutComponent, loadChildren: () => import('./patient/patient.module').then(m => m.PatientModule) },
    { path: 'shared', loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
