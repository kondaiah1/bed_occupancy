import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SharedComponent } from './shared.component';
import { BeddetailsComponent } from './component/beddetails/beddetails.component';
// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';
 
// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as charts from "fusioncharts/fusioncharts.powercharts";
import { FusionchartComponent } from './component/fusionchart/fusionchart.component';
// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme, charts);

@NgModule({
  declarations: [SharedComponent, BeddetailsComponent, FusionchartComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    FusionChartsModule
  ],
  exports: [BeddetailsComponent, FusionchartComponent, FusionChartsModule]
})
export class SharedModule { }
