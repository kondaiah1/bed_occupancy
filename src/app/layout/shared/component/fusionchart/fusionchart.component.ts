import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fusionchart',
  templateUrl: './fusionchart.component.html',
  styleUrls: ['./fusionchart.component.scss']
})
export class FusionchartComponent implements OnInit {
  @Input() fusionChart: any;

  constructor() { }

  ngOnInit() {
    
  }

}
