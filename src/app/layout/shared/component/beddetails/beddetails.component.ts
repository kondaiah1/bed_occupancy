import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-beddetails',
  templateUrl: './beddetails.component.html',
  styleUrls: ['./beddetails.component.scss']
})
export class BeddetailsComponent implements OnInit {
  @Input() bedDetail: any;

  constructor() { }

  ngOnInit() {
    
  }

}
