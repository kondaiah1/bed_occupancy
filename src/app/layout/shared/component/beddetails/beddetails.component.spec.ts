import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeddetailsComponent } from './beddetails.component';

describe('BeddetailsComponent', () => {
  let component: BeddetailsComponent;
  let fixture: ComponentFixture<BeddetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeddetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeddetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
