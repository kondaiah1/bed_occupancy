import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './common/component/header/header.component';
import { SidemenuComponent } from './common/component/sidemenu/sidemenu.component';
import { FooterComponent } from './common/component/footer/footer.component';
import { MaterialModule } from '../core/module/material/material.module';
import { CommonCoreModule } from '../core/module/common/common.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  declarations: [LayoutComponent, HeaderComponent, SidemenuComponent, FooterComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    MaterialModule,
    CommonCoreModule,
    SharedModule
  ],
  exports: [SharedModule]
})
export class LayoutModule { }
