import { Component, OnInit } from '@angular/core';
import { scatterChartObject } from '../common/models/charts/scatter';
import { StorageService } from 'src/app/core/auth/storage/storage.service';
import { bedDetailsObject } from '../common/models/beddetails';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  public bedDetailsObject:any = new bedDetailsObject();
  public chartObject = new scatterChartObject();
  private graphData:any;

  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.graphData = JSON.parse(this.storageService.getSessionStorage('graphData'));
    if (this.graphData) {
      this.patientBedSuccess(this.graphData);
    }
  }

  //after success of patient bed API
  patientBedSuccess(data:any) {
    let chartCategory = [],
      dataSet = [];

    //bed details data object after API response
    this.bedDetailsObject.seriesTransaction = data.number_of_transactions;
    this.bedDetailsObject.parameterAnalyzed = data.number_of_parameters;
    this.bedDetailsObject.bedGreater = data.number_of_beds_more_than_70;
    this.bedDetailsObject.bedLesser = data.number_of_beds_less_than_70;

    data.Number_of_Patients_Waiting_to_be_Seen_ED.forEach((item:string, index:number) => {
      dataSet.push({ x: item, y: data.free_beds[index]});
    });
    
    //making graph data object after API response
    this.chartObject.dataSource.chart.caption = "";
    this.chartObject.dataSource.chart.subcaption = "";
    this.chartObject.dataSource.chart.xAxisName = "Number of Patients Waiting to be Seen (ED)";
    this.chartObject.dataSource.chart.YAxisName = "Free Beds";
    this.chartObject.dataSource.chart.theme = "fusion";
    this.chartObject.dataSource.chart.showLegend = 1;
    this.chartObject.dataSource.chart.legendPosition = "bottom";
    this.chartObject.dataSource.chart.interactiveLegend = 0;
    this.chartObject.dataSource.chart.legendIconSides = 4;
    this.chartObject.dataSource.categories[0].verticallinealpha = "20";
    this.chartObject.dataSource.categories[0].category = chartCategory;
    this.chartObject.dataSource.dataset[0].seriesname = "Beds";
    this.chartObject.dataSource.dataset[0].anchorsides = 0;
    this.chartObject.dataSource.dataset[0].anchorradius = 4;
    this.chartObject.dataSource.dataset[0].color = '#5d62b5';
    this.chartObject.dataSource.dataset[0].anchorbordercolor = '#5d62b5';
    this.chartObject.dataSource.dataset[0].anchorbgcolor = '#5d62b5';
    this.chartObject.dataSource.dataset[0].data = dataSet;
  }

}
