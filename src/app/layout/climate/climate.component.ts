import { Component, OnInit } from '@angular/core';
import { boxWhiskerChartObject } from '../common/models/charts/boxandwhisker';
import { StorageService } from 'src/app/core/auth/storage/storage.service';
import { climateGraphsArray } from './constant/climate';
import { bedDetailsObject } from '../common/models/beddetails';

@Component({
  selector: 'app-climate',
  templateUrl: './climate.component.html',
  styleUrls: ['./climate.component.scss']
})
export class ClimateComponent implements OnInit {
  public bedDetailsObject:any = new bedDetailsObject();
  public chartObject:any;
  private graphData:any;
  private graphArr:any = climateGraphsArray;
  private temperatureGraph:any;

  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.graphData = JSON.parse(this.storageService.getSessionStorage('graphData'));
    if (this.graphData) {
      this.climateBedSuccess(this.graphData);
    }
    this.chartObject = this.getGraphShow('', 'Temperature');
  }

  private getGraphShow(graphObj:any, graphLabel:any) {
    switch (graphLabel) {
      case 'Temperature':
          return this.temperatureGraph = this.temperatureGraph ? this.temperatureGraph : graphObj;
    }
  }

  //making graph data object after API response
  private setGraphValue(graphItem:any, graphObj:any, chartCategory:any, data:any) {
    graphObj.dataSource.chart.caption = "";
    graphObj.dataSource.chart.subcaption = "";
    graphObj.dataSource.chart.xAxisName = graphItem.xAxisName;
    graphObj.dataSource.chart.YAxisName = graphItem.yAxisName;
    graphObj.dataSource.chart.theme = "fusion";
    graphObj.dataSource.chart.showLegend = 1;
    graphObj.dataSource.chart.legendPosition = "bottom";
    graphObj.dataSource.chart.interactiveLegend = 0;
    graphObj.dataSource.chart.legendIconSides = 4;
    graphObj.dataSource.categories[0].category = chartCategory;
    graphObj.dataSource.dataset[0].lowerboxcolor = "#0075c2";
    graphObj.dataSource.dataset[0].upperboxcolor = "#1aaf5d";
    graphObj.dataSource.dataset[0].data = data;
    //set graph data based up on charts
    this.getGraphShow(graphObj, graphItem.xLabel);
  }
  
  private climateBedSuccess (response:any) {
    //bed details data object after API response
    this.bedDetailsObject.seriesTransaction = response.number_of_transactions;
    this.bedDetailsObject.parameterAnalyzed = response.number_of_parameters;
    this.bedDetailsObject.bedGreater = response.number_of_beds_more_than_70;
    this.bedDetailsObject.bedLesser = response.number_of_beds_less_than_70;

    for (const graphItem of this.graphArr) {
      let graphObj = new boxWhiskerChartObject();
      let chartCategory:any = [],
          data:any = [];
      response[graphItem.xLabel].filter((element:any, hourIndex:number, array:any) => {
        let elementVal = element.toString();
          if(array.indexOf(element) === hourIndex) {
              chartCategory.push({ label: elementVal });
              data.push({
                  value: (response[graphItem.yLabel][hourIndex]).toString()
              })
          } else {
              let getChartCategoryIndex = chartCategory.findIndex((item:any) => item.label === elementVal);
              data[getChartCategoryIndex].value = data[getChartCategoryIndex].value + ',' + response[graphItem.yLabel][hourIndex];
          }
      });
      //making graph data object after API response
      this.setGraphValue(graphItem, graphObj, chartCategory, data);
    }
  }

}
