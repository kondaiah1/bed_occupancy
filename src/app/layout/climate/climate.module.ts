import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClimateRoutingModule } from './climate-routing.module';
import { ClimateComponent } from './climate.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [ClimateComponent],
  imports: [
    CommonModule,
    ClimateRoutingModule,
    SharedModule
  ]
})
export class ClimateModule { }
