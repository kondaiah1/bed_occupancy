import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidenavService } from './service/sidenav/sidenav.service';
import { layoutObject } from './common/models/layout';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  parentData = new layoutObject();
  menuOptions: any = {};
  @ViewChild('sidenav', {static: true}) public sidenav: MatSidenav;

  constructor(private sideNavService: SidenavService) { }

  ngOnInit() {
    this.parentData.innerWidth = window.innerWidth;
    //this.windowWidth = window.innerWidth;
    this.menuShowHide(); //call function menu hide and show based up on window size
    //menu toggle by service
    this.sideNavService.sideNavToggleSubject.subscribe(()=> {
      this.sidenav.toggle();
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event:any) {
    this.parentData.innerWidth = window.innerWidth;
    this.menuShowHide(); //call function menu hide and show based up on window size
  }

  //menu hide and show based up on window size
  menuShowHide() {
    let windowWidth = this.parentData.innerWidth;
    this.menuOptions = this.parentData.menuOptions;
    if (windowWidth && windowWidth < 992) {
      this.menuOptions.open = false;
      this.menuOptions.mode = "mode";
    } else if (windowWidth && windowWidth > 992) {
      this.menuOptions.open = true;
      this.menuOptions.mode = "side";
    }
  }

}
