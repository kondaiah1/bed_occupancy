import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  //set value into session storage
  public setSessionStorage(key:string, value:string) {
    return sessionStorage.setItem(key, value);
  }

  //get value into session storage
  public getSessionStorage(key:string) {
    return sessionStorage.getItem(key);
  }

  //remove value into session storage
  public removeSessionStorage(key:string) {
    return sessionStorage.removeItem(key);
  }

  //clear all session storage
  public clearSessionStorage() {
    return sessionStorage.clear();
  }

  //set value into local storage
  public setLocalStorage(key:string, value:string) {
    return localStorage.setItem(key, value);
  }

  //get value into local storage
  public getLocalStorage(key:string) {
    return localStorage.getItem(key);
  }

  //remove value into local storage
  public removeLocalStorage(key:string) {
    return localStorage.removeItem(key);
  }

  //clear all local storage
  public clearLocalStorage() {
    return localStorage.clear();
  }

}
