import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router,
    private storageService: StorageService) { }

  logout() {
    this.storageService.clearSessionStorage();
    this.storageService.clearLocalStorage();
    this.router.navigate(['/login'])
  }
}
